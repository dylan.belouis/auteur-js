"use client";
import React, { useEffect, useState } from "react";
import Section from "../Section";
import {
  Card,
  CardContent,
  TextField,
  Button,
  Typography,
  Box,
} from "@mui/material";
import emailjs from "@emailjs/browser";

export default function ContactForm() {
  const [formData, setFormData] = useState({
    name: "",
    email: "",
    message: "",
  });
  const [successMessage, setSuccessMessage] = useState("");

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  useEffect(() => {
    emailjs.init("ypvFngrNUHq2I6VhO"); // Remplacez YOUR_PUBLIC_KEY par votre clé publique
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    // Logique d'envoi ici
    emailjs
      .send("service_9ii4cvj", "template_f3fuxws", {
        from_name: formData.name,
        message: formData.message,
        reply_to: formData.email,
      })
      .then((success) => {
        setSuccessMessage("Message envoyé avec succès !");
        console.log("success", success);
      })
      .catch((e) => {
        setSuccessMessage(
          "Erreur lors de l'envoi du message. Veuillez réessayer."
        );
        console.log("erreur", e);
      });
    setFormData({
      name: "",
      email: "",
      message: "",
    });
  };

  return (
    <Section color="#F4D35E" skew wrapperSx={{ mb: 4 }}>
      <Typography
        variant="h5"
        component="h2"
        gutterBottom
        sx={{
          textAlign: "center",
          fontFamily: "Great Vibes, serif",
          fontWeight: 700,
          fontSize: "2rem",
          color: "#083d77",
        }}
      >
        Contactez-nous
      </Typography>
      <form id="contact" onSubmit={handleSubmit} noValidate autoComplete="off">
        <TextField
          label="Nom"
          name="name"
          value={formData.name}
          onChange={handleChange}
          fullWidth
          margin="normal"
          required
          sx={{
            "& > *": {
              fontSize: "1.5rem",
              textAlign: "center",
              fontFamily: "Great Vibes, serif !important",
              fontWeight: 700,
              color: "#083d77",
              backgroundColor: "white",
            },
          }}
        />
        <TextField
          label="Email"
          type="email"
          name="email"
          value={formData.email}
          onChange={handleChange}
          fullWidth
          margin="normal"
          required
          sx={{
            "& > *": {
              fontSize: "1.5rem",
              textAlign: "center",
              fontFamily: "Great Vibes, serif !important",
              fontWeight: 700,
              color: "#083d77",
              backgroundColor: "white",
            },
          }}
        />
        <TextField
          label="Message"
          name="message"
          value={formData.message}
          onChange={handleChange}
          multiline
          rows={4}
          fullWidth
          margin="normal"
          required
          sx={{
            "& > *": {
              fontSize: "1.5rem",
              textAlign: "center",
              fontFamily: "Great Vibes, serif !important",
              fontWeight: 700,
              color: "#083d77",
              backgroundColor: "white",
            },
          }}
        />
        <Box display="flex" justifyContent="center">
          <Button
            type="submit"
            variant="outlined"
            sx={{
              mt: 2,
              mx: "auto",
              backgroundColor: "#083d77",
              color: "#F4D35E",
            }}
          >
            Envoyer
          </Button>
        </Box>
      </form>
      <Typography>{successMessage}</Typography>
    </Section>
  );
}
