import React from "react";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Section from "../Section";

export default function Footer() {
  const currentYear = new Date().getFullYear();

  return (
    <div>
      <Box
        sx={{
          padding: "20px",
          textAlign: "center",
          borderTop: "1px solid #eaeaea",
          width: "100%",
        }}
      >
        <Typography
          sx={{
            margin: 0,
            padding: "5px",
            fontSize: "14px",
            color: "#666",
          }}
        >
          © {currentYear} Joëlle Soyer. Tous droits réservés.
        </Typography>
      </Box>
    </div>
  );
}
