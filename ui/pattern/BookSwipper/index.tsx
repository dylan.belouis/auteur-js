"use client";
import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import { Pagination } from "swiper/modules";
import { books } from "@/ui/data/books";
import Section from "../Section";
import BookCard from "@/ui/store/book/BookCard";
import { Box, Typography } from "@mui/material";
import styles from "./index.module.css"; // Assurez-vous de créer un fichier CSS correspondant

export default function BookSlider() {
  return (
    <Section skew color="white">
      <Box
        sx={{
          padding: "0 2rem",
        }}
        id="books"
      >
        <Typography
          variant="h5"
          component="h2"
          sx={{
            margin: "4rem 0",
            width: "100%",
            textAlign: "center",
            fontFamily: "Great Vibes, serif",
            fontWeight: 700,
            fontSize: "2rem",
            color: "#083d77",
          }}
        >
          Mes livres
        </Typography>
        <Swiper
          slidesPerView={1}
          spaceBetween={30}
          pagination={{
            clickable: true,
          }}
          modules={[Pagination]}
          className={styles.mySwiper}
          breakpoints={{
            768: {
              slidesPerView: 3,
            },
          }}
        >
          {books.map((book, index) => (
            <SwiperSlide key={index}>
              <BookCard book={book} />
            </SwiperSlide>
          ))}
        </Swiper>
      </Box>
    </Section>
  );
}
