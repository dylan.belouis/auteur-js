import React from "react";
import styles from "./index.module.css"; // Assurez-vous de créer un fichier CSS correspondant

export default function Banner({ title, backgroundImage }) {
  return (
    <div
      className={styles.banner}
      style={{ backgroundImage: `url(${backgroundImage})` }}
    >
      <h1>{title}</h1>
    </div>
  );
}
