"use client";
// @ts-nocheck
import React from "react";
import {
  Document,
  Page,
  Text,
  View,
  StyleSheet,
  PDFDownloadLink,
} from "@react-pdf/renderer";

// Définition des styles pour le PDF
const styles = StyleSheet.create({
  page: {
    padding: 30,
    fontSize: 12,
    fontFamily: "Helvetica",
  },
  header: {
    fontSize: 20,
    marginBottom: 20,
    textAlign: "center",
  },
  section: {
    marginBottom: 10,
  },
  table: {
    // @ts-ignore
    display: "table",
    width: "auto",
    borderStyle: "solid",
    borderWidth: 1,
    borderRightWidth: 0,
    borderBottomWidth: 0,
  },
  tableRow: {
    flexDirection: "row",
  },
  tableColHeader: {
    width: "25%",
    borderStyle: "solid",
    borderWidth: 1,
    borderLeftWidth: 0,
    borderTopWidth: 0,
    backgroundColor: "#bdbdbd",
    padding: 5,
  },
  tableCol: {
    width: "25%",
    borderStyle: "solid",
    borderWidth: 1,
    borderLeftWidth: 0,
    borderTopWidth: 0,
    padding: 5,
  },
  tableCellHeader: {
    fontWeight: "bold",
  },
  tableCell: {},
});

// Composant qui définit le document PDF de la facture
const InvoiceDocument = ({ invoiceData }) => (
  <Document>
    <Page style={styles.page}>
      <Text style={styles.header}>FACTURE</Text>

      {/* Informations générales */}
      <View style={styles.section}>
        <Text>Date : {invoiceData.date}</Text>
        <Text>Facture n° : {invoiceData.invoiceNumber}</Text>
      </View>

      {/* Informations client */}
      <View style={styles.section}>
        <Text>Client : {invoiceData.clientName}</Text>
        <Text>Adresse : {invoiceData.clientAddress}</Text>
      </View>

      {/* Liste des articles */}
      <View style={styles.section}>
        <Text style={{ marginBottom: 5 }}>Articles :</Text>
        <View style={styles.table}>
          <View style={styles.tableRow}>
            <View style={styles.tableColHeader}>
              <Text style={styles.tableCellHeader}>Produit</Text>
            </View>
            <View style={styles.tableColHeader}>
              <Text style={styles.tableCellHeader}>Quantité</Text>
            </View>
            <View style={styles.tableColHeader}>
              <Text style={styles.tableCellHeader}>Prix unitaire</Text>
            </View>
            <View style={styles.tableColHeader}>
              <Text style={styles.tableCellHeader}>Total</Text>
            </View>
          </View>
          {invoiceData.items.map((item, idx) => (
            <View style={styles.tableRow} key={idx}>
              <View style={styles.tableCol}>
                <Text style={styles.tableCell}>{item.product}</Text>
              </View>
              <View style={styles.tableCol}>
                <Text style={styles.tableCell}>{item.quantity}</Text>
              </View>
              <View style={styles.tableCol}>
                <Text style={styles.tableCell}>{item.unitPrice}€</Text>
              </View>
              <View style={styles.tableCol}>
                <Text style={styles.tableCell}>
                  {(item.unitPrice * item.quantity).toFixed(2)}€
                </Text>
              </View>
            </View>
          ))}
        </View>
      </View>

      {/* Total */}
      <View style={styles.section}>
        <Text style={{ fontSize: 16, fontWeight: "bold" }}>
          Total à payer : {invoiceData.total}€
        </Text>
      </View>
    </Page>
  </Document>
);

// Composant qui affiche un lien pour télécharger la facture au format PDF
const Invoice = ({ invoiceData }) => (
  <div>
    <PDFDownloadLink
      document={<InvoiceDocument invoiceData={invoiceData} />}
      fileName="facture.pdf"
      style={{
        textDecoration: "none",
        padding: "10px 20px",
        color: "#fff",
        backgroundColor: "#3f51b5",
        borderRadius: "4px",
      }}
    >
      {/* @ts-ignore */}
      {({ loading }) =>
        loading ? "Chargement du document..." : "Télécharger la facture"
      }
    </PDFDownloadLink>
  </div>
);

// Exemple d'utilisation avec des données fictives
const invoiceData = {
  date: "22/02/2025",
  invoiceNumber: "INV-2025-001",
  clientName: "Jean Dupont",
  clientAddress: "123 Rue de la République, 75000 Paris, France",
  items: [
    {
      product: "Produit A",
      quantity: 2,
      unitPrice: 10,
    },
    {
      product: "Produit B",
      quantity: 1,
      unitPrice: 20,
    },
  ],
  total: (2 * 10 + 1 * 20).toFixed(2),
};

const InvoiceApp = () => (
  <div style={{ textAlign: "center", marginTop: "20rem" }}>
    <Invoice invoiceData={invoiceData} />
  </div>
);

export default InvoiceApp;
