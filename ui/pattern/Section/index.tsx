import type { BoxProps, SxProps, Theme } from "@mui/material";
import { Box, Breakpoint, Container } from "@mui/material";

import type { ResponsiveStyleValue, SystemStyleObject } from "@mui/system";
import type { Property } from "csstype";

export interface SectionProps extends Omit<BoxProps, "color"> {
  color?:
    | SystemStyleObject<Theme>
    | ResponsiveStyleValue<string[] | Property.BackgroundColor | undefined>;
  wrapperSx?: SxProps<Theme>;
  backdropSx?: SxProps<Theme>;
  component?: React.ElementType;
  skew?: boolean;
  containerMaxWidth?: Breakpoint | undefined;
}

export default function Section({
  color = "white",
  component = "section",
  containerMaxWidth = "lg",
  sx,
  wrapperSx,
  backdropSx,
  skew,
  children,
  ...props
}: SectionProps) {
  return (
    <Box
      sx={{
        position: "relative",
        py: 8,
        ...wrapperSx,
      }}
      component={component}
    >
      <Box
        maxWidth={containerMaxWidth}
        component={Container} // Content
        sx={{
          position: "relative",
          zIndex: 1,
          ...sx,
        }}
        {...props}
      >
        {children}
      </Box>

      <Box // Background Rotated Box
        sx={{
          width: "113%",
          height: "100%",
          position: "absolute",
          top: "50%",
          zIndex: 0,
          left: "50%",
          transform: skew
            ? "translate(-50%, -50%) rotate(-2.035deg)"
            : "translate(-50%, -50%)",
          flexShrink: "0",
          borderRadius: "0.50475rem",
          bgcolor: color,
          ...backdropSx,
        }}
      />
    </Box>
  );
}
