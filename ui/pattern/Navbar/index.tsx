"use client";
import React, { useState } from "react";
import styles from "./index.module.css";
import Link from "next/link";

const Navbar = ({ colorAfter = true }) => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <nav
      className={
        colorAfter
          ? styles.Navbar_nav
          : isOpen
          ? styles.Navbar_nav
          : styles.Navbar_nav2
      }
      id="navbar"
    >
      <div className={styles.Navbar_div_Test}>
        <div className={styles.Navbar_div_invisible}></div>
        <div className={styles.Navbar_general_display}>
          <div className={styles.logo}>
            <Link href="/" className={styles.logo}>
              Joelle Soyer
            </Link>
          </div>
          <div id="wrapper" className={styles.Navbar_div_menu}>
            <ul className={styles.Navbar_ul_menu} id="Navbar_ul">
              <li className={styles.Navbar_li_menu}>
                <Link
                  className={colorAfter ? styles.different : styles.different2}
                  id="different"
                  href="/"
                >
                  Acceuil
                </Link>
                {/* <Link  href="/">
              Acceuil
            </Link> */}
              </li>
              <li className={styles.Navbar_li_menu}>
                <div
                  className={colorAfter ? styles.different : styles.different2}
                  id="different"
                >
                  <Link href="/store">Boutique</Link>
                </div>
              </li>
              <li className={styles.Navbar_li_menu}>
                <div
                  className={colorAfter ? styles.different : styles.different2}
                  id="different"
                >
                  <Link href="/panier">Mon panier</Link>
                </div>
              </li>
              <li className={styles.Navbar_li_menu}>
                <div
                  className={colorAfter ? styles.different : styles.different2}
                  id="different"
                >
                  <Link href="/#about">A propos</Link>
                </div>
              </li>
              <li className={styles.Navbar_li_menu}>
                <Link href="#contact">Contact</Link>
              </li>
            </ul>
          </div>
          <div
            className={isOpen ? styles.opened : styles.menu}
            onClick={() => setIsOpen(!isOpen)}
          >
            {/* <button
              className={!isOpen ? styles.opened : styles.menu}
              onclick="this.classList.toggle('opened');this.setAttribute('aria-expanded', this.classList.contains('opened'))"
              aria-label="Main Menu"
            > */}
            <svg width="50%" height="auto" viewBox="0 0 100 100">
              <path
                className={colorAfter ? styles.line1Black : styles.line1}
                d="M 20,29.000046 H 80.000231 C 80.000231,29.000046 94.498839,28.817352 94.532987,66.711331 94.543142,77.980673 90.966081,81.670246 85.259173,81.668997 79.552261,81.667751 75.000211,74.999942 75.000211,74.999942 L 25.000021,25.000058"
              />
              <path
                className={colorAfter ? styles.line1Black : styles.line2}
                d="M 20,50 H 80"
              />
              <path
                className={colorAfter ? styles.line1Black : styles.line3}
                d="M 20,70.999954 H 80.000231 C 80.000231,70.999954 94.498839,71.182648 94.532987,33.288669 94.543142,22.019327 90.966081,18.329754 85.259173,18.331003 79.552261,18.332249 75.000211,25.000058 75.000211,25.000058 L 25.000021,74.999942"
              />
            </svg>
            {/* </button> */}
            {/* <Image
              src={
                !colorAfter
                  ? isOpen
                    ? "/close.png"
                    : "/burgerMenu.png"
                  : isOpen
                  ? "/close.png"
                  : "/darkBurgerMenu.png"
              }
              id="logo"
              alt="logo"
              width={!isOpen ? 45 : 30}
              height={!isOpen ? 50 : 30}
              className={styles.Navbar_logo_mobile_menu}
            /> */}
          </div>
        </div>
      </div>
      {isOpen && (
        <div className={styles.Navbar_div_menu_mobile_content}>
          <ul className={styles.Navbar_ul_menu_mobile}>
            <li
              className={styles.Navbar_li_menu_mobile}
              onClick={() => setIsOpen(false)}
            >
              <Link
                // className={colorAfter ? styles.different : styles.different2}
                id="different"
                href="/"
              >
                Accueil
              </Link>
            </li>
            <li
              className={styles.Navbar_li_menu_mobile}
              onClick={() => setIsOpen(false)}
            >
              <Link href="/store">Boutique</Link>
            </li>
            <li className={styles.Navbar_li_menu_mobile}>
              <div
                className={colorAfter ? styles.different : styles.different2}
                id="different"
              >
                <Link href="/panier">Mon panier</Link>
              </div>
            </li>
            <li
              className={styles.Navbar_li_menu_mobile}
              onClick={() => setIsOpen(false)}
            >
              <Link href="/#about">A propos</Link>
            </li>
            <li
              className={styles.Navbar_li_menu_mobile}
              onClick={() => setIsOpen(false)}
            >
              <Link href="#contact">Contact</Link>
            </li>
          </ul>
        </div>
      )}
    </nav>
  );
};

export default Navbar;

{
  /* <div className={styles.logo}>
<Link href="/">Joelle Soyer</Link>
</div>
<div
className={isOpen ? styles.opened : styles.menu}
onClick={toggleMenu}
>
<svg width="100" height="100" viewBox="0 0 100 100">
  <path
    className={styles.line1Black}
    d="M 20,29.000046 H 80.000231 C 80.000231,29.000046 94.498839,28.817352 94.532987,66.711331 94.543142,77.980673 90.966081,81.670246 85.259173,81.668997 79.552261,81.667751 75.000211,74.999942 75.000211,74.999942 L 25.000021,25.000058"
  />
  <path className={styles.line1Black} d="M 20,50 H 80" />
  <path
    className={styles.line1Black}
    d="M 20,70.999954 H 80.000231 C 80.000231,70.999954 94.498839,71.182648 94.532987,33.288669 94.543142,22.019327 90.966081,18.329754 85.259173,18.331003 79.552261,18.332249 75.000211,25.000058 75.000211,25.000058 L 25.000021,74.999942"
  />
</svg>
</div>
<div className={styles.Navbar_div_menu_mobile_content}>
<Link href="/">Accueil</Link>
<Link href="/store">Boutique</Link>
<Link href="/#about">A propos</Link>
<Link href="#contact">Contact</Link> */
}
