// Presentation.js

import React from "react";
import { Typography, Box } from "@mui/material";
import Section from "../Section";

export default function Presentation() {
  return (
    <Section color="#F4D35E" skew>
      <Box
        sx={{
          "& > h2": {
            m: "4rem 0",
            width: "100%",
            textAlign: "center",
          },
        }}
      >
        <Typography
          variant="h5"
          component="h2"
          gutterBottom
          sx={{
            fontFamily: "Great Vibes, serif",
            fontWeight: 700,
            fontSize: "2rem",
            color: "#083d77",
          }}
        >
          À propos de Joëlle Soyer
        </Typography>
        <Box id="about" sx={{ p: 2 }}>
          {" "}
          {/* Adjust the padding as needed */}
          <Typography paragraph color="#083d77">
            Originaire de la Mayenne, Joëlle Soyer a élu domicile en Touraine
            depuis 1979. Après une carrière dans le commerce, elle se consacre à
            l'écriture, sa passion depuis cinq ans.
          </Typography>
          <Typography paragraph color="#083d77">
            Son œuvre débute par un roman autobiographique poignant, «&nbsp;Le
            fruit de l’indifférence&nbsp;», qui explore son enfance au sein de
            l'assistance publique. Cette première publication voit le jour en
            2019 grâce aux Éditions Dechartres.
          </Typography>
          <Typography paragraph color="#083d77">
            L'année suivante, Joëlle Soyer s'aventure dans la littérature
            jeunesse avec «&nbsp;Yomeji, le hérisson, le déménagement&nbsp;», un
            récit captivant destiné aux plus jeunes. Elle poursuit sur cette
            lancée en 2021 avec la conclusion de l'histoire de Yomeji,
            «&nbsp;Les amours de sa vie&nbsp;», clôturant ainsi avec brio cette
            série attendrissante, également publiée aux éditions Dechartres.
          </Typography>
        </Box>
      </Box>
    </Section>
  );
}
