"use client";
import React, { useState, useEffect } from "react";
import { Box, Button, Typography } from "@mui/material";
import {
  CardElement,
  useStripe,
  useElements,
  Elements,
} from "@stripe/react-stripe-js";
import { loadStripe } from "@stripe/stripe-js";
import { useRouter } from "next/navigation";
import { db } from "../../app/utils/firebaseConfig";
import { doc, updateDoc } from "firebase/firestore";

// Remplacez "your-public-key-here" par votre clé publique Stripe via la variable d'environnement
const stripePromise = loadStripe(
  process.env.NEXT_PUBLIC_STRIPE_PUBLIC_KEY ?? ""
);

const CheckoutForm = ({ id }) => {
  const stripe = useStripe();
  const elements = useElements();
  const router = useRouter();

  const [errorMessage, setErrorMessage] = useState("");
  const [processing, setProcessing] = useState(false);
  const [paymentSucceeded, setPaymentSucceeded] = useState(false);

  useEffect(() => {
    if (paymentSucceeded) {
      // Redirige vers la page de confirmation après 2 secondes
      setTimeout(() => {
        router.push(`/panier/${id}/checkout-informations/avoice`);
      }, 2000);
    }
  }, [paymentSucceeded, router]);

  const handleSubmit = async (event) => {
    event.preventDefault();
    setProcessing(true);

    if (!stripe || !elements) {
      setProcessing(false);
      return;
    }

    const cardElement = elements.getElement(CardElement);
    // @ts-ignore
    const { error, paymentMethod } = await stripe.createPaymentMethod({
      type: "card",
      card: cardElement,
    });

    if (error) {
      setErrorMessage(error.message ?? "");
      setProcessing(false);
    } else {
      console.log("PaymentMethod créé :", paymentMethod);

      // Récupérer l'ID de la commande depuis localStorage

      if (id) {
        try {
          const orderRef = doc(db, "orders", id);
          // Mettre à jour le statut de la commande et ajouter la date de validation
          await updateDoc(orderRef, {
            status: "validated",
            validatedAt: new Date(),
          });
          console.log("Commande mise à jour avec validation");
        } catch (err) {
          console.error("Erreur lors de la mise à jour de la commande:", err);
        }
      } else {
        console.error("Aucun ID de commande trouvé dans localStorage");
      }

      setPaymentSucceeded(true);
      setErrorMessage("");
      setProcessing(false);
    }
  };

  return (
    <Box
      sx={{
        mt: "10rem",
        maxWidth: 500,
        mx: "auto",
        p: 3,
        border: "1px solid #ccc",
        borderRadius: 2,
        boxShadow: 3,
      }}
    >
      <Typography variant="h5" sx={{ mb: 2, textAlign: "center" }}>
        Paiement par carte
      </Typography>
      <form onSubmit={handleSubmit}>
        <Box
          sx={{
            border: "1px solid #e0e0e0",
            borderRadius: 1,
            p: 2,
            mb: 2,
          }}
        >
          <CardElement
            options={{
              style: {
                base: {
                  fontSize: "16px",
                  color: "#424770",
                  "::placeholder": {
                    color: "#aab7c4",
                  },
                },
                invalid: {
                  color: "#9e2146",
                },
              },
            }}
          />
        </Box>
        {errorMessage && (
          <Typography variant="body2" color="error" sx={{ mb: 2 }}>
            {errorMessage}
          </Typography>
        )}
        <Button
          type="submit"
          variant="contained"
          color="primary"
          fullWidth
          disabled={!stripe || processing || paymentSucceeded}
        >
          {processing ? "Traitement..." : "Payer"}
        </Button>
        {paymentSucceeded && (
          <Typography
            variant="body1"
            sx={{ mt: 2, color: "green", textAlign: "center" }}
          >
            Paiement réussi !
          </Typography>
        )}
      </form>
    </Box>
  );
};

// Composant wrapper qui enveloppe CheckoutForm avec Elements
const CheckoutFormWrapper = ({ id }) => {
  return (
    <Elements stripe={stripePromise}>
      <CheckoutForm id={id} />
    </Elements>
  );
};

export default CheckoutFormWrapper;
