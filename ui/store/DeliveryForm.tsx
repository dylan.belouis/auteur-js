"use client";
import React, { useState, useEffect } from "react";
import { Box, Button, Grid, TextField, Typography } from "@mui/material";
import Link from "next/link";
import { db } from "../../app/utils/firebaseConfig";
import { doc, getDoc, updateDoc } from "firebase/firestore";

const DeliveryForm = ({ id }) => {
  const [formData, setFormData] = useState({
    firstName: "",
    lastName: "",
    email: "",
    phone: "",
    address1: "",
    address2: "",
    city: "",
    postalCode: "",
    country: "",
  });

  const [loading, setLoading] = useState(false);
  const [orderId, setOrderId] = useState(id || null);

  // Récupération de la commande existante depuis Firestore
  useEffect(() => {
    if (id) {
      const fetchOrder = async () => {
        try {
          const orderRef = doc(db, "orders", id);
          const orderSnap = await getDoc(orderRef);
          if (orderSnap.exists()) {
            const orderData = orderSnap.data();
            setFormData({
              firstName: orderData.firstName || "",
              lastName: orderData.lastName || "",
              email: orderData.email || "",
              phone: orderData.phone || "",
              address1: orderData.address1 || "",
              address2: orderData.address2 || "",
              city: orderData.city || "",
              postalCode: orderData.postalCode || "",
              country: orderData.country || "",
            });
          } else {
            console.error("Commande non trouvée");
          }
        } catch (error) {
          console.error("Erreur en récupérant la commande :", error);
        }
      };
      fetchOrder();
    }
  }, [id]);

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);

    try {
      if (!id) {
        console.error("Aucun identifiant de commande fourni");
        return;
      }
      // Met à jour la commande existante dans Firestore
      const orderRef = doc(db, "orders", id);
      await updateDoc(orderRef, {
        ...formData,
        status: "pending", // Vous pouvez définir d'autres champs si nécessaire
        updatedAt: new Date(),
      });
      setOrderId(id);
      console.log("Commande mise à jour avec ID:", id);
    } catch (error) {
      console.error("Erreur lors de la mise à jour de la commande :", error);
    } finally {
      setLoading(false);
    }
  };

  return (
    <Box
      sx={{
        maxWidth: 600,
        margin: "auto",
        mt: "10rem",
        p: 2,
        border: "1px solid #ccc",
        borderRadius: 2,
        boxShadow: 3,
      }}
    >
      <Typography variant="h4" sx={{ mb: 2, textAlign: "center" }}>
        Informations de Livraison
      </Typography>
      <form onSubmit={handleSubmit}>
        <Grid container spacing={2}>
          {/* Prénom et Nom */}
          <Grid item xs={12} sm={6}>
            <TextField
              label="Prénom"
              name="firstName"
              fullWidth
              value={formData.firstName}
              onChange={handleChange}
              required
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              label="Nom"
              name="lastName"
              fullWidth
              value={formData.lastName}
              onChange={handleChange}
              required
            />
          </Grid>
          {/* Adresse email */}
          <Grid item xs={12}>
            <TextField
              label="Adresse email"
              name="email"
              fullWidth
              value={formData.email}
              onChange={handleChange}
              required
              type="email"
            />
          </Grid>
          {/* Téléphone */}
          <Grid item xs={12}>
            <TextField
              label="Téléphone"
              name="phone"
              fullWidth
              value={formData.phone}
              onChange={handleChange}
              required
              type="tel"
            />
          </Grid>
          {/* Adresse ligne 1 */}
          <Grid item xs={12}>
            <TextField
              label="Adresse"
              name="address1"
              fullWidth
              value={formData.address1}
              onChange={handleChange}
              required
            />
          </Grid>
          {/* Complément d'adresse */}
          <Grid item xs={12}>
            <TextField
              label="Complément d'adresse"
              name="address2"
              fullWidth
              value={formData.address2}
              onChange={handleChange}
            />
          </Grid>
          {/* Ville et Code postal */}
          <Grid item xs={12} sm={6}>
            <TextField
              label="Ville"
              name="city"
              fullWidth
              value={formData.city}
              onChange={handleChange}
              required
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              label="Code postal"
              name="postalCode"
              fullWidth
              value={formData.postalCode}
              onChange={handleChange}
              required
            />
          </Grid>
          {/* Pays */}
          <Grid item xs={12}>
            <TextField
              label="Pays"
              name="country"
              fullWidth
              value={formData.country}
              onChange={handleChange}
              required
            />
          </Grid>
          {/* Bouton de validation */}
          <Grid item xs={12}>
            <Button
              type="submit"
              variant="contained"
              color="primary"
              fullWidth
              disabled={loading}
            >
              {loading
                ? "Mise à jour en cours..."
                : "Mettre à jour la commande"}
            </Button>
          </Grid>
        </Grid>
      </form>
      <Box sx={{ textAlign: "center", mt: 2 }}>
        <Link
          href={`/panier/${orderId}/checkout-informations/payment`}
          passHref
        >
          <Button variant="contained" color="secondary">
            Passer au paiement
          </Button>
        </Link>
      </Box>
    </Box>
  );
};

export default DeliveryForm;
