"use client";
import React, { useEffect, useState } from "react";
import {
  Box,
  Button,
  Typography,
  Card,
  CardContent,
  CardActions,
} from "@mui/material";
import { FaTrashAlt } from "react-icons/fa";
import { books } from "../data/books";
import { useRouter } from "next/navigation";
import { db, collection, addDoc } from "../../app/utils/firebaseConfig"; // ajustez le chemin selon votre structure

const Cart = () => {
  const [cart, setCart] = useState<any>([]);
  const router = useRouter();

  // Supprimer un article du panier
  const removeFromCart = (id) => {
    const newCart = cart.filter((item) => item.id !== id);
    setCart(newCart);
    window.localStorage.setItem("cart", JSON.stringify(newCart));
  };

  // Vider le panier
  const clearCart = () => {
    setCart([]);
    window.localStorage.removeItem("cart");
  };

  // Récupérer le livre correspondant à l'ID de l'article du panier
  const getBookDetails = (id) => {
    return books.find((book) => book.id === id.toString());
  };

  // Calcul du total
  const total = cart.reduce((sum, item) => {
    const book = getBookDetails(item.id);
    return book ? sum + book.price * item.quantity : sum;
  }, 0);

  useEffect(() => {
    const data = window.localStorage.getItem("cart");
    if (data) {
      try {
        setCart(JSON.parse(data));
      } catch (error) {
        console.error("Error parsing cart data:", error);
      }
    }
  }, []);

  // Fonction pour créer la commande sur Firebase et rediriger vers la page de paiement
  const handleValidateCart = async () => {
    // Construction de l'objet commande
    const orderData = {
      items: cart.map((item) => ({
        productId: item.id,
        quantity: item.quantity,
      })),
      total,
      createdAt: new Date(),
      // Vous pouvez ajouter d'autres infos (statut, infos client, etc.)
    };

    try {
      // Enregistrement dans la collection "orders" de Firestore
      const docRef = await addDoc(collection(db, "orders"), orderData);
      console.log("Commande créée avec ID:", docRef.id);
      // On vide le panier après la création
      setCart([]);
      window.localStorage.removeItem("cart");
      // Redirection vers la page /panier/{orderId}/checkout-informations
      router.push(`/panier/${docRef.id}/checkout-informations`);
    } catch (error) {
      console.error("Erreur lors de la création de la commande :", error);
    }
  };

  return (
    <Box
      sx={{
        p: 2,
        pt: "10rem",
        maxWidth: 600,
        margin: "auto",
      }}
    >
      <Typography variant="h4" sx={{ mb: 4 }}>
        🛒 Mon Panier
      </Typography>

      {cart.length === 0 ? (
        <Typography variant="body1" sx={{ color: "gray" }}>
          Votre panier est vide.
        </Typography>
      ) : (
        <>
          <Box sx={{ display: "flex", flexDirection: "column", gap: 2 }}>
            {cart.map((item) => {
              const book = getBookDetails(item.id);
              return (
                <Card
                  key={item.id}
                  sx={{
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center",
                    p: 2,
                  }}
                >
                  <CardContent sx={{ flex: 1 }}>
                    <Typography variant="h6">
                      {book ? book.title : "Produit inconnu"} x {item.quantity}
                    </Typography>
                    <Typography variant="body2">
                      {(book?.price || 0) * item.quantity}€
                    </Typography>
                  </CardContent>
                  <CardActions>
                    <Button
                      onClick={() => removeFromCart(item.id)}
                      sx={{ color: "red" }}
                    >
                      <FaTrashAlt />
                    </Button>
                  </CardActions>
                </Card>
              );
            })}
          </Box>
          <Typography variant="h6" sx={{ mt: 2 }}>
            Total: {total}€
          </Typography>
          <Box sx={{ display: "flex", justifyContent: "space-between", mt: 2 }}>
            <Button variant="contained" color="error" onClick={clearCart}>
              Vider le panier
            </Button>
            <Button
              variant="contained"
              color="primary"
              onClick={handleValidateCart}
            >
              Valider mon panier
            </Button>
          </Box>
        </>
      )}
    </Box>
  );
};

export default Cart;
