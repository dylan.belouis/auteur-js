"use client";
import styles from "./index.module.css";
import Image from "next/image";
import React, { useState } from "react";

export default function CoverViewer({ book }) {
  const [visibleImage, setVisibleImage] = useState(book.cover);

  const handleChange = (image) => {
    setVisibleImage(image);
  };

  return (
    <div
      style={{
        margin: "4rem 1rem",
      }}
    >
      <Image
        src={visibleImage}
        style={{ position: "relative" }}
        alt={`Couverture du livre ${book.title}`}
        className={styles.bookCover}
        width={300}
        height={400}
      />
      <div className={styles.coverImages}>
        <Image
          src={book.cover}
          style={{ position: "relative" }}
          alt={`Couverture du livre ${book.title}`}
          className={styles.bookCover}
          onClick={() => handleChange(book.cover)}
          width={100}
          height={130}
        />
        {book.backCover && (
          <Image
            src={book.backCover}
            onClick={() => handleChange(book.backCover)}
            style={{ position: "relative" }}
            alt={`Couverture du livre ${book.title}`}
            className={styles.bookCover}
            width={100}
            height={130}
          />
        )}
      </div>
    </div>
  );
}
