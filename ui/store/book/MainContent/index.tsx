import styles from "./index.module.css";
import React from "react";
import CoverViewer from "../CoverViewer";
import Informations from "../Informations";
import ShopCard from "../ShopCard";
import Section from "../../../pattern/Section";
import AddToCart from "../../AddToCart";

export default function MainContent({ book }) {
  return (
    <Section color="white" skew>
      <div className={styles.mainContent}>
        <CoverViewer book={book} />
        <Informations book={book} />
        <AddToCart id={book.id} name={book.title} price={book.price} />
      </div>
    </Section>
  );
}
