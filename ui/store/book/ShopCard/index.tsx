"use client";
import React, { useState } from "react";
import { loadStripe } from "@stripe/stripe-js";
import {
  Elements,
  CardElement,
  useStripe,
  useElements,
} from "@stripe/react-stripe-js";

// Charge Stripe avec ta clé publique
const stripePromise = loadStripe("pk_test_1234567890abcdef"); // Remplace par ta clé publique Stripe

const CheckoutForm = () => {
  const stripe = useStripe();
  const elements = useElements();
  const [loading, setLoading] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const [success, setSuccess] = useState(false);

  const handleSubmit = async (event: React.FormEvent) => {
    event.preventDefault();
    setLoading(true);

    if (!stripe || !elements) return;

    const cardElement = elements.getElement(CardElement);
    if (!cardElement) return;

    try {
      const { paymentIntent, error } = await stripe.confirmCardPayment(
        "pi_1234567890_secret",
        {
          payment_method: {
            card: cardElement,
          },
        }
      );

      if (error) {
        setErrorMessage(error.message || "Erreur lors du paiement");
        setSuccess(false);
      } else if (paymentIntent?.status === "succeeded") {
        setSuccess(true);
      }
    } catch (error) {
      setErrorMessage("Une erreur est survenue lors du paiement.");
      setSuccess(false);
    }

    setLoading(false);
  };

  return (
    <form
      onSubmit={handleSubmit}
      className="space-y-4 p-4 border rounded-lg shadow-md"
    >
      <CardElement className="p-2 border rounded-md" />
      {errorMessage && <p className="text-red-500">{errorMessage}</p>}
      {success ? (
        <p className="text-green-500 font-bold">Paiement réussi !</p>
      ) : (
        <button
          type="submit"
          className="bg-blue-500 text-white px-4 py-2 rounded-md hover:bg-blue-600 disabled:opacity-50"
          disabled={!stripe || loading}
        >
          {loading ? "Paiement en cours..." : "Payer"}
        </button>
      )}
    </form>
  );
};

const StripePayment = () => {
  return (
    <Elements stripe={stripePromise}>
      <CheckoutForm />
    </Elements>
  );
};

export default StripePayment;
