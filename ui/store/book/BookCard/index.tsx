import React from "react";
import Link from "next/link";
import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import Image from "next/image";

export default function BookCard({ book }) {
  return (
    <Link
      href={`/store/${book.id}`}
      passHref
      style={{
        width: "max-content",
      }}
    >
      <Card
        sx={{
          width: { xs: 300, md: 345 },
          boxShadow: "0 0 10px rgba(0, 0, 0, 0.1)",
          borderRadius: "1rem",
          // m: 1, // margin
          display: "flex",
          flexDirection: "column",
          height: "100%",
          backgroundColor: "#083d77",
        }}
      >
        <CardMedia
          component="div"
          sx={{
            position: "relative",
            height: 180,
            width: "100%",
            "&:hover": {
              cursor: "pointer",
            },
          }}
        >
          <Image
            src={book.cover}
            alt={`Couverture du livre ${book.title}`}
            layout="fill"
            objectFit="cover" // Adapter l'image à la taille du CardMedia
          />
        </CardMedia>
        <CardContent sx={{ flexGrow: 1, textAlign: "center" }}>
          <Typography gutterBottom variant="h5" component="div" color="#f4d35e">
            {book.title}
          </Typography>
          <Typography variant="body2" color="#f4d35e">
            {book.year}
          </Typography>
          <Typography variant="body2" color="#f4d35e">
            {book.subtitle}
          </Typography>
        </CardContent>
      </Card>
    </Link>
  );
}
