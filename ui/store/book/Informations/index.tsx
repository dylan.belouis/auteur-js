import styles from "./index.module.css";
import Image from "next/image";
import React from "react";

export default function Informations({ book }) {
  return (
    <div
      style={{
        margin: "4rem 1rem",
      }}
    >
      <div className={styles.bookInfo}>
        <h3 style={{ color: "black" }}>{book.title}</h3>
        <b>
          <p style={{ color: "black" }}>{book.price} €</p>
        </b>
        <p>{book.year}</p>
        <p>{book.description}</p>
        <b>
          <p>Editions {book.editor}</p>
        </b>
      </div>
    </div>
  );
}
