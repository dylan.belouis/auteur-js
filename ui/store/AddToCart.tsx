"use client";
import { Check } from "@mui/icons-material";
import { Alert, Button, Snackbar } from "@mui/material";
import React, { useState } from "react";
import { useCart } from "./CartContext";

const AddToCart = ({
  id,
  name,
  price,
}: {
  id: number;
  name: string;
  price: number;
}) => {
  const { addToCart } = useCart();
  const [open, setOpen] = useState(false);

  const handleClick = () => {
    addToCart(id);
    setOpen(true);
  };

  return (
    <div className="p-4 border rounded-lg shadow-md bg-white">
      <Button onClick={handleClick} variant="contained">
        Ajouter au panier
      </Button>
      <Snackbar
        open={open}
        autoHideDuration={3000}
        onClose={() => setOpen(false)}
        // message="Note archived"
        anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
      >
        <Alert
          icon={<Check fontSize="inherit" />}
          severity="success"
          sx={{ backgroundColor: "#083d77", color: "#f4d35e" }}
        >
          Nous avons bien ajouté votre article dans votre panier
        </Alert>
      </Snackbar>
    </div>
  );
};

export default AddToCart;
