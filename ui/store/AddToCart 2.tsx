"use client";
import React from "react";
import { useCart } from "./CartContext";

const AddToCart = ({
  id,
  name,
  price,
}: {
  id: number;
  name: string;
  price: number;
}) => {
  const { addToCart } = useCart();

  return (
    <div className="p-4 border rounded-lg shadow-md bg-white">
      <h3 className="text-lg font-semibold">{name}</h3>
      <p>{price}€</p>
      <button
        onClick={() => addToCart(id)}
        className="bg-green-500 text-white px-3 py-1 mt-2 rounded-md hover:bg-green-600"
      >
        Ajouter au panier
      </button>
    </div>
  );
};

export default AddToCart;
