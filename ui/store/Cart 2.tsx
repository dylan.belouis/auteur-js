"use client";
import React from "react";
import { useCart } from "./CartContext";
import { FaTrashAlt } from "react-icons/fa";

// Liste des produits (à remplacer par un vrai backend plus tard)
const PRODUCTS = [
  { id: 1, name: "Produit A", price: 10 },
  { id: 2, name: "Produit B", price: 20 },
  { id: 3, name: "Produit C", price: 30 },
];

const Cart = () => {
  const { cart, removeFromCart, clearCart } = useCart();

  // Récupérer les infos des produits en fonction de l'ID
  const getProductDetails = (id: number) => PRODUCTS.find((p) => p.id === id);

  const total = cart.reduce((sum, item) => {
    const product = getProductDetails(item.id);
    return product ? sum + product.price * item.quantity : sum;
  }, 0);

  return (
    <div className="p-4 border rounded-lg shadow-lg max-w-md mx-auto bg-white">
      <h2 className="text-xl font-bold mb-4">🛒 Mon Panier</h2>

      {cart.length === 0 ? (
        <p className="text-gray-500">Votre panier est vide.</p>
      ) : (
        <>
          <ul>
            {cart.map((item) => {
              const product = getProductDetails(item.id);
              return (
                <li
                  key={item.id}
                  className="flex justify-between items-center py-2 border-b"
                >
                  <span>
                    {product?.name} x{item.quantity}
                  </span>
                  <span>{(product?.price ?? 0) * item.quantity}€</span>
                  <button
                    onClick={() => removeFromCart(item.id)}
                    className="text-red-500 hover:text-red-700"
                  >
                    {/* @ts-ignore */}
                    <FaTrashAlt />
                  </button>
                </li>
              );
            })}
          </ul>

          <p className="mt-4 font-bold">Total: {total}€</p>

          <div className="mt-4 flex justify-between">
            <button
              onClick={clearCart}
              className="bg-red-500 text-white px-3 py-1 rounded-md hover:bg-red-600"
            >
              Vider le panier
            </button>
            <button className="bg-blue-500 text-white px-3 py-1 rounded-md hover:bg-blue-600">
              Payer avec Stripe
            </button>
          </div>
        </>
      )}
    </div>
  );
};

export default Cart;
