import styles from "./index.module.css";
import Image from "next/image";
import React from "react";
import { books } from "@/ui/data/books";
import Link from "next/link";
import BookCard from "../book/BookCard";
import Section from "../../pattern/Section";

export default function Books() {
  return (
    <Section color="#F4D35E" skew>
      <div className={styles.books} id="books">
        {books.map((book, index) => (
          <BookCard book={book} key={`book-${index}`} />
        ))}
      </div>
    </Section>
  );
}
