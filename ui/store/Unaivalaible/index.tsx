import styles from "./index.module.css";
import Image from "next/image";
import React from "react";

export default function Unaivalaible() {
  return (
    <div
      style={{
        margin: "10rem 0",
        width: "100%",
        textAlign: "center",
      }}
    >
      <h2>Bientot disponible</h2>
    </div>
  );
}
