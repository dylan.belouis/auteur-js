"use client";
import React from "react";
import { Box, Button, Typography, Card, CardContent } from "@mui/material";
import {
  PDFDownloadLink,
  Document,
  Page,
  Text,
  View,
  StyleSheet,
} from "@react-pdf/renderer";
import { books } from "../data/books";

// Styles pour le document PDF
const pdfStyles = StyleSheet.create({
  page: {
    padding: 30,
    fontSize: 12,
    fontFamily: "Helvetica",
  },
  header: {
    fontSize: 18,
    marginBottom: 10,
    textAlign: "center",
  },
  section: {
    marginBottom: 10,
  },
  tableRow: {
    flexDirection: "row",
  },
  tableCol: {
    width: "25%",
    borderStyle: "solid",
    borderWidth: 1,
    padding: 5,
  },
});

// Composant qui génère le document PDF de la facture
const InvoiceDocument = ({ orderData, id, validatedAt }) => (
  <Document>
    <Page style={pdfStyles.page}>
      <Text style={pdfStyles.header}>FACTURE</Text>
      {/* Informations générales */}
      <View style={pdfStyles.section}>
        <Text>Date de finalisation : {validatedAt}</Text>
        <Text>Numéro de commande : {id}</Text>
      </View>
      {/* Informations client */}
      <View style={pdfStyles.section}>
        <Text>
          Client : {orderData.firstName} {orderData.lastName}
        </Text>
        <Text>Adresse : {orderData.address1 + " " + orderData.address2}</Text>
      </View>
      {/* Détail des articles */}
      <View style={pdfStyles.section}>
        <Text>Articles :</Text>
        {orderData.items &&
          orderData.items.map((item, index) => {
            const article = books.filter(
              (book) => book.id === item.productId
            )[0];
            return (
              <View style={pdfStyles.tableRow} key={index}>
                <Text style={pdfStyles.tableCol}>{article.title}</Text>
                <Text style={pdfStyles.tableCol}>{item.quantity}</Text>
                <Text style={pdfStyles.tableCol}>{article.price}€</Text>
                <Text style={pdfStyles.tableCol}>
                  {(item.quantity * item.unitPrice).toFixed(2)}€
                </Text>
              </View>
            );
          })}
      </View>
      {/* Total */}
      <View style={pdfStyles.section}>
        <Text style={{ fontSize: 14, fontWeight: "bold" }}>
          Total : {orderData.total}€
        </Text>
      </View>
    </Page>
  </Document>
);

// Composant FinalCheckoutInformation
const FinalCheckoutInformation = ({ orderData, id }) => {
  const createdAt = orderData.createdAt?.toDate
    ? orderData.createdAt.toDate().toLocaleDateString()
    : orderData.createdAt;
  const validatedAt = orderData.validatedAt?.toDate
    ? orderData.validatedAt.toDate().toLocaleDateString()
    : orderData.validatedAt;
  return (
    <Box
      sx={{
        mt: "10rem",
        p: 3,
        maxWidth: 600,
        mx: "auto",
        textAlign: "center",
        border: "1px solid #ccc",
        borderRadius: 2,
        boxShadow: 3,
      }}
    >
      <Typography variant="h4" sx={{ mb: 2 }}>
        Félicitations !
      </Typography>
      <Typography variant="body1" sx={{ mb: 2 }}>
        Votre commande a bien été enregistrée.
      </Typography>

      <Card sx={{ mb: 2 }}>
        <CardContent>
          <Typography variant="h6" sx={{ mb: 1 }}>
            Récapitulatif de la commande
          </Typography>
          <Typography variant="body2">
            <strong>Numéro de commande :</strong> {id}
          </Typography>
          <Typography variant="body2">
            <strong>Date de finalisation :</strong> {validatedAt}
          </Typography>
          <Typography variant="body2">
            <strong>Client :</strong> {orderData.firstName} {orderData.lastName}
          </Typography>
          <Typography variant="body2">
            <strong>Adresse :</strong>{" "}
            {orderData.address1 + " " + orderData.address2}
          </Typography>
          <Typography variant="body2">
            <strong>Total à payer :</strong> {orderData.total}€
          </Typography>
        </CardContent>
      </Card>

      <PDFDownloadLink
        document={
          <InvoiceDocument
            orderData={orderData}
            id={id}
            validatedAt={validatedAt}
          />
        }
        fileName="facture.pdf"
        style={{
          textDecoration: "none",
          padding: "10px 20px",
          color: "#fff",
          backgroundColor: "#3f51b5",
          borderRadius: "4px",
          display: "inline-block",
        }}
      >
        {/* @ts-ignore */}
        {({ loading }) =>
          loading ? "Génération de la facture..." : "Télécharger la facture"
        }
      </PDFDownloadLink>
    </Box>
  );
};

export default FinalCheckoutInformation;
