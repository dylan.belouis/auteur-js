export const books = [
  {
    deliveryPrice: 7,
    id: "2",
    title: "Quel monde nous prépare-t-on pour demain ?",
    price: 17,
    year: 2023,
    cover: "/Quel-monde-nous-reserve-front.jpeg",
    backCover: "/Quel-monde-nous-reserve-back.jpeg",
    categorie: "Autobiographie",
    editor: "Dechartres",
    subtitle: "les marchands de peur ont-ils trop de pouvoir ?",
    description: `Essai paru aux Editions Dechartres en octobre 2023.
    L’Auteure observe : avec la multiplication des réseaux sociaux, les citoyens ont
    l’impression d’être mieux informés.
    Des téléspectateurs pensent détenir la vérité en écoutant les « informations ».
    S’ils le disent à la télé, çà doit être vrai !
    Les sujets brûlants comme la Covid, le réchauffement climatique, l’immigration,
    l’électricité, l’inflation provoquent des fractures politiques, mais aussi des
    tensions dans les familles.
    Pourquoi certains faits sont-ils exclus du traitement médiatique traditionnel ?
    Joëlle Soyer explore du côté de ceux qui n’ont pas voix au chapitre.
    Un essai qui bouscule bien des certitudes.`,
  },
  {
    deliveryPrice: 7,
    id: "1",
    title: "Le fruit de l’indifférence",
    price: 17,
    year: 2019,
    cover: "/le-fruit-de-l-indiference-front.jpeg",
    backCover: "/le-fruit-de-l-indiference-back.jpeg",
    categorie: "Autobiographie",
    editor: "Dechartres",
    subtitle:
      "Un roman autobiographique retraçant une enfance à l'assistance publique.",
    description: `Roman autobiographique, paru aux Editions Dechartres, en mai 2019,
      commence par décrire, de façon poétique, le monde sensible de l'enfance où
      libellules, cerisiers et autres éléments qui pourraient figurer dans une comptine
      sont confrontés aux brutalités psychiques et physiques.
      C'est un orage de mésaventures qui s'abat sur cette petite fille qui deviendra
      grande, emportant avec elle son lot de violence conjugale. Joëlle Soyer décrit en
      189 pages les différents fardeaux supportés et comment elle a réussi à s'en
      alléger en travaillant et en réussissant`,
  },
  {
    deliveryPrice: 7,
    id: "4",
    title: "Yomeji, le hérisson, sa vie de famille",
    price: 8,
    year: 2021,
    cover: "/sa-vie-de-fammile.jpeg",
    backCover: "/yomeji-vie-de-famille-back.jpeg",
    categorie: "Jeunesse",
    editor: "Dechartres",
    subtitle:
      "Un livre pour enfants racontant les aventures de Yomeji, un hérisson attachant.",
    description: `Yomeji se promenait dans la campagne an caquetant, en râlant, en cherchant
    quelques baies qu’il affectionnait particulièrement.
    Il aimait sa hérissonne et ses quatre petits, mais il aimait tout autant séduire à
    nouveau, le hérisson n’est pas fidèle, il aime plaire.
    Il avait aperçu une belle hérissonne lors de sa promenade et s’évertuait à la
    retrouver. Il participait ainsi à la préservation de son espèce en voie de
    disparition. Les écoliers apprécieront les illustrations de Nicolas Boisbouvier,
    l’Illustrateur.`,
  },
  {
    deliveryPrice: 7,
    id: "6",
    title: "Yomeji, le hérisson, le déménagement",
    price: 8,
    year: 2020,
    cover: "/yomeji-le-demenagement.jpeg",
    categorie: "Jeunesse",
    editor: "Dechartres",
    subtitle:
      "Un livre pour enfants racontant les aventures de Yomeji, un hérisson attachant.",
    description: `Une découverte ludique et pédagogique sur la vie des hérissons dans la nature.
    Espèce protégée depuis 1981. C’est l’histoire d’un hérisson à la recherche d’une
    maisonnette pour y emménager avec son épouse Lily qui attend un heureux
    évènement.
    Mais chacun sait que ce petit mammifère qui rend de nombreux services aux
    jardiniers se met vite en danger lorsqu’il doit traverser une route.
    Le jardinier Papi Louis, désolé d’une mésaventure qu’il a fait vivre à Yomeji
    sans le vouloir, décide de construire une maisonnette à l’abri du vent ou la
    famille pourra s’abriter.`,
  },
];
