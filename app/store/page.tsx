import Banner from "@/ui/pattern/Banner/index";
import styles from "./page.module.css";
import Presentation from "@/ui/pattern/Presentation/index";
import BookSlider from "@/ui/pattern/BookSwipper/index";
import ContactForm from "@/ui/pattern/Contact";
import Books from "@/ui/store/Books";
import { CartProvider } from "../../ui/store/CartContext";

export default function Home() {
  return (
    <main className={styles.main}>
      <CartProvider>
        <Banner title="Boutique" backgroundImage="/soyer.JPG" />
        <Books />
        <ContactForm />
      </CartProvider>
    </main>
  );
}
