import Banner from "@/ui/pattern/Banner/index";
import styles from "./page.module.css";
import ContactForm from "@/ui/pattern/Contact";
import { books } from "@/ui/data/books";
import Unaivalaible from "@/ui/store/Unaivalaible";
import MainContent from "@/ui/store/book/MainContent";
import { CartProvider } from "../../../ui/store/CartContext";

export default function Page({ params }) {
  const { id } = params;
  const book = books.filter((book) => book.id === id)[0];

  return (
    <main className={styles.main}>
      <CartProvider>
        <Banner title={book?.title} backgroundImage="/soyer.JPG" />
        <MainContent book={book} />
        <ContactForm />
      </CartProvider>
    </main>
  );
}
