import styles from "./page.module.css";
import ContactForm from "@/ui/pattern/Contact";
import { books } from "@/ui/data/books";
import DeliveryForm from "@/ui/store/DeliveryForm";

export default function Page({ params }) {
  const { id } = params;
  const book = books.filter((book) => book.id === id)[0];

  console.log(id);
  return (
    <main className={styles.main}>
      <DeliveryForm id={id} />
      <ContactForm />
    </main>
  );
}
