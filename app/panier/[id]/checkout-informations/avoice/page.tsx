"use client";
import React, { useState, useEffect } from "react";
import Banner from "@/ui/pattern/Banner/index";
import styles from "./page.module.css";
import dynamic from "next/dynamic";
import AvoiceApp from "@/ui/pattern/Avoice";
import ContactForm from "@/ui/pattern/Contact";
import { db } from "../../../../utils/firebaseConfig"; // Ajustez le chemin selon votre projet
import { doc, getDoc } from "firebase/firestore";
import { Typography } from "@mui/material";

// Import dynamique avec SSR désactivé pour éviter les problèmes côté serveur
const FinalCheckoutInformation = dynamic(
  () => import("../../../../../ui/store/FinalCheckoutInformation"),
  { ssr: false }
);

export default function Page({ params }) {
  const { id } = params; // L'id correspond à l'id de la commande dans Firestore
  const [orderData, setOrderData] = useState<any>(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    if (id) {
      const fetchOrder = async () => {
        try {
          const orderRef = doc(db, "orders", id);
          const orderSnap = await getDoc(orderRef);
          if (orderSnap.exists()) {
            const data = orderSnap.data();
            // Transformation des champs en fonction de ceux ajoutés en base

            console.log(data, "datadatadatadatadatadata");
            const updatedOrderData = {
              clientName: `${data.firstName || ""} ${
                data.lastName || ""
              }`.trim(),
              email: data.email || "",
              phone: data.phone || "",
              address: `${data.address1 || ""}${
                data.address2 ? ", " + data.address2 : ""
              }, ${data.city || ""}, ${data.postalCode || ""}, ${
                data.country || ""
              }`,
              status: data.status || "",
              createdAt: data.createdAt?.toDate
                ? data.createdAt.toDate().toLocaleDateString()
                : data.createdAt,
              validatedAt: data.validatedAt?.toDate
                ? data.validatedAt.toDate().toLocaleDateString()
                : data.validatedAt,
              // Ajoutez d'autres champs si nécessaire
            };
            console.log(
              updatedOrderData,
              "updatedOrderDataupdatedOrderDataupdatedOrderDataupdatedOrderData"
            );
            setOrderData(data);
          } else {
            console.error("Commande non trouvée");
          }
        } catch (error) {
          console.error(
            "Erreur lors de la récupération de la commande :",
            error
          );
        } finally {
          setLoading(false);
        }
      };
      fetchOrder();
    } else {
      setLoading(false);
    }
  }, [id]);

  return (
    <main className={styles.main} style={{ marginTop: "10rem" }}>
      {loading ? (
        <Typography variant="body1" sx={{ textAlign: "center", mt: 4 }}>
          Chargement de la commande...
        </Typography>
      ) : orderData ? (
        <FinalCheckoutInformation orderData={orderData} id={id} />
      ) : (
        <Typography
          variant="body1"
          color="error"
          sx={{ textAlign: "center", mt: 4 }}
        >
          Commande non trouvée
        </Typography>
      )}
      <ContactForm />
    </main>
  );
}
