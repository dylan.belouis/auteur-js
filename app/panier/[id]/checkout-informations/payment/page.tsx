import Banner from "@/ui/pattern/Banner/index";
import styles from "./page.module.css";
import AvoiceApp from "@/ui/pattern/Avoice";
import { books } from "@/ui/data/books";
import FinalCheckoutInformation from "../../../../../ui/store/FinalCheckoutInformation";
import OrderForm from "@/ui/store/CheckoutForm";
import ContactForm from "@/ui/pattern/Contact";
import CheckoutForm from "@/ui/store/CheckoutForm";

export default function Page({ params }) {
  const { id } = params;

  return (
    <main className={styles.main}>
      <CheckoutForm id={id} />
      <ContactForm />
    </main>
  );
}
